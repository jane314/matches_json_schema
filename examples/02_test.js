import { assert } from "https://deno.land/std@0.110.0/testing/asserts.ts";

import {
  matches_json_schema,
  matches_obj_schema,
} from "../matches_json_schema.js";

let schema_json = `
{
	"a": {
		"a": {
			"a:": {
				"num": 273
			}
		}
	}	
}
`;

// Matching Object
let obj = {
	a: { a: { a: (num) => num % 3 === 0 } }
};

Deno.test("test #4 - nested + numerical test", () => matches_json_schema(schema_json, obj)); // Prints true

let schema = undefined;

Deno.test("test #5 - undefined !== null", () => !matches_obj_schema(schema, null));

