import { assert } from "https://deno.land/std@0.110.0/testing/asserts.ts";

import {
  matches_json_schema,
  matches_obj_schema,
} from "../matches_json_schema.js";

let schema_json = `
{
	"a": 0,
	"b": {
		"c": "",
		"d": 0,
		"e": null
	}
}
`;

// Matching Object
let obj = {
  a: 34,
  b: {
    c: "hello",
    d: 12,
    e: null,
    f: [],
  },
  c: [undefined, 4],
};

Deno.test("test #1 - schema", () => matches_json_schema(schema_json, obj)); // Prints true

// Schema
let schema = [0];

// Matching Object
obj = [2, 3, 5, 7, 11, 13];

Deno.test("test #2 - schema", () => matches_obj_schema(schema, obj)); // Prints true

// Schema
schema = {
	name: '',
	date: str => (new Date(str)).toString() !== 'Invalid Date'
}

obj = {
	name: 'Janie',
	date: 'oct 10, 2021 11:44 AM UTC-5'
};

Deno.test("test #3 - schema with function validation", () => matches_obj_schema(schema, obj)); // Prints true
